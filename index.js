// Server Setup
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");


const index = express();
const port = process.env.PORT || 4000;

// Mongoose Connection
mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://michaelbajun:09225585319@224-oneza.yigp5un.mongodb.net/E-commerce-API?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

db.on("error", () => console.error("Connection Error!"));
db.once("open", () => console.log("You don't have to worry, you're now connected to MongoDB!"));

// Middlewares
index.use(cors());
index.use(express.json());
index.use(express.urlencoded({extended: true}));

// Main URI
index.use("/users", userRoutes);
index.use("/products", productRoutes);





index.listen(port, () => {console.log(`API is now running at port ${[port]}!`)});