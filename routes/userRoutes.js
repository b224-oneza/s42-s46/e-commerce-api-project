const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth");

// Routes
// Route for User Registration
router.post("/register", (req,res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for User Authentication
router.post("/login", (req,res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for User (Non-Admin) Checkout
router.post("/checkout", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        orderId: req.body.orderId,
        productId: req.body.productId,
        productName: userData.productName,
        quantity: req.body.quantity,
        totalAmount: req.body.totalAmount
    }
    userController.checkout(data).then(resultFromController => res.send(resultFromController))
});

// Route for Retrieve User Details
router.post("/userProfile", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    console.log(req.headers.authorization);

    userController.userProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


module.exports = router;