const express = require("express");
const router = express.Router();
const productController =  require("../controllers/productController");
const auth = require("../auth");

// Routes
// Routes for creating a product sample
router.post("/createProduct", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    console.log(userData);
    console.log(req.headers.authorization);

    productController.createProduct(req.body, userData).then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all products
router.get("/allProducts", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    console.log(userData);
    console.log(req.headers.authorization);

    productController.allProducts(userData).then(resultFromController => res.send(resultFromController))
});


// Route for retrieving all active products
router.get("/allActiveProducts", (req, res) => {

    productController.allActiveProducts().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving single product
router.get("/specificProduct/:productId", (req, res) => {
    console.log(req.params.productId)

    productController.specificProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Route for updating product sample
router.put("/updateProduct/:productId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

   if(userData.isAdmin) {
        productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
   } else {
        res.send({auth: "failed"})
   }
});

// Route for archiving a product sample
router.put("/archiveProduct/:productId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin) {
        productController.archiveProduct(req.params, req.body).then(resultFromController => res.send (resultFromController))
    } else {
        res.send({auth: "failed"})
    }
});

// Route for activating a product sample
router.put("/activateProduct/:productId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin) {
        productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
    } else {
        res.send({auth: "failed"})
    }
});


module.exports = router;