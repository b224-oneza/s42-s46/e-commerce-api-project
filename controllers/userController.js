const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),

    })
    
    return newUser.save().then((_user, error) => {
        if(error) {
            return false
        } else {
            return true
        }
    })

};

// User Authentication
module.exports.loginUser = (reqBody) => {
    return User.findOne({email:reqBody.email}).then(result => {
        if(result == null) {
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect) {
                return {access: auth.createAccessToken(result)}
            } else {
                return false
            }
        }
    })
};

// User create order (non-admin)
module.exports.checkout = async (data) => {
    if(data.isAdmin == true) {
        return false
    } else {
        let isUserUpdated = await User.findById(data.userId).then(user => {

            user.orderedProduct.push({productId: data.productId, productName: data.productName, quantity: data.quantity, totalAmount: data.totalAmount});
            return user.save().then((_user, error) => {
                if(error) {
                    return false
                } else {
                    return true
                }
            })
        });
        
        let isProductUpdated = await Product.findById(data.productId).then(product => {

            product.userOrder.push({userId: data.userId, orderId: data.orderId});

            return product.save().then((_product, error) => {
                if(error) {
                    return false
                } else {
                    return true
                }
            })
        });

        if(isUserUpdated && isProductUpdated) {
            return true
        } else {
            return false
        };
    }
        
}; 

// User Details 
module.exports.userProfile = (userData) => {
    return User.findById(userData.id).then(result => {
        console.log(result);

        if(result == null) {
            return false
        } else {
            result.password = "*****"
            return result
        }
    })
};