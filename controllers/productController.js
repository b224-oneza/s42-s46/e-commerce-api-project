const { update } = require("../models/Product");
const Product = require("../models/Product");

// Create Product Sample
module.exports.createProduct = (reqBody, userData) => {

    if(userData.isAdmin = true) {
        let newProduct = new Product({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        });

        return newProduct.save().then((product, error) => {
            if(error) {
                return false;
            } else {
                return true;
            }
        });
    } else {
        return false;
    }
};

// Retrieve All Product Sample
module.exports.allProducts = (data) => {

    if(data.isAdmin) {
        return Product.find({}).then(result => {
            return result
        });
        
    } else {
        return false
    }
};
 
// Retrieving All Active Products
module.exports.allActiveProducts = () => {

    return Product.find({isActive: true}).then(result => {
        return result
    })
};

// Retrieving Single Product
module.exports.specificProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then(result => {
        return result
    })
};

// Updating Single Product Information
module.exports.updateProduct = (reqParams, reqBody) => {

    let updateProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((updateProduct, error) => {

        console.log(updateProduct)
        if(error) {
            return false
        } else {
            return true
        }
    })
};

// Archiving Single Product
module.exports.archiveProduct = (reqParams, reqBody) => {

    let archiveProduct = {
        isActive: reqBody.isActive
    }

    return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((archiveProduct, error) => {

        console.log(archiveProduct)
        if(error) {
            return false
        } else {
            return true
        }
    })
};

// Activating Single Product
module.exports.activateProduct = (reqParams, reqBody) => {

    let activateProduct = {
        isActive: reqBody.isActive
    }

    return Product.findByIdAndUpdate(reqParams.productId, activateProduct).then((activateProduct, error) => {

        console.log(activateProduct)
        if(error) {
            return false
        } else {
            return true
        }
    })
}